<?php
/**
 * Implements hook_views_handlers()
 */
function makemeeting_views_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'makemeeting_views'),
    ),
    'handlers' => array(
      // The name of my handler
      'makemeeting_views_handler_makemeeting_answers' => array(
        // The name of the handler we are extending.
        'parent' => 'views_handler_field',
      ),
    ),
  );
}
/**
 * Implements hook_views_data().
 */
function makemeeting_views_views_data() {
  $data['makemeeting_answers']['table']['group'] = t('Makemeeting answers');
  // Advertise this table as a possible base table
  $data['makemeeting_answers']['table']['base'] = array(
    'title' => t('Makemeeting answers'),
    'help' => t('Contains makemeeting answers we want to expose to views.'),
  );
  $data['makemeeting_answers']['answer_id'] = array(
    'title' => t('Answer ID'),
    'help' => t('The answer record primary ID.'),
    'field' => array(
      'click sortable' => TRUE,
      'handler' => 'views_handler_field_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );
  $data['makemeeting_answers']['field_name'] = array(
    'title' => t('Field name'),
    'help' => t('The name of the field this data is attached to.'),
    'field' => array(
      'click sortable' => TRUE,
      'handler' => 'views_handler_field',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort'
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string'
    ),
  );
  $data['makemeeting_answers']['entity_type'] = array(
    'title' => t('Entity type'),
    'help' => t('The entity type this data is attached to'),
    'field' => array(
      'click sortable' => TRUE,
      'handler' => 'views_handler_field',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort'
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string'
    ),
  );
  $data['makemeeting_answers']['deleted'] = array(
    'title' => t('Deleted'),
    'help' => t('A boolean indicating whether this data item has been deleted'),
    'field' => array(
      'click sortable' => TRUE,
      'handler' => 'views_handler_field_boolean',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort'
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator'
    ),
  );
  $data['makemeeting_answers']['entity_id'] = array(
    'title' => t('Entity ID'),
    'help' => t('The entity id this data is attached to'),
    'field' => array(
      'click sortable' => TRUE,
      'handler' => 'views_handler_field_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_node_nid',
    ),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'node',
      'base field' => 'nid',
      'label' => t('Node'),
    ),
  );
  $data['makemeeting_answers']['uid'] = array(
    'title' => t('User ID'),
    'help' => t('The user id of the user who answered'),
    'field' => array(
      'click sortable' => TRUE,
      'handler' => 'views_handler_field_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_user_uid',
    ),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'users',
      'base field' => 'uid',
      'field' => 'uid',
      'label' => t('Users'),
    ),
  );
  $data['makemeeting_answers']['name'] = array(
    'title' => t('Name'),
    'help' => t('The name provided when answering.'),
    'field' => array(
      'click sortable' => TRUE,
      'handler' => 'views_handler_field',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort'
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string'
    ),
  );
  $data['makemeeting_answers']['value'] = array(
    'title' => t('Values'),
    'help' => t('The answers value rendered differently'),
    'field' => array(
      'handler' => 'makemeeting_views_handler_makemeeting_answers',
    ),
  );
  return $data;
}
?>
