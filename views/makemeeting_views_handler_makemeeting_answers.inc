<?php
/**
 * Provides a custom views field with display options.
 */
class makemeeting_views_handler_makemeeting_answers extends views_handler_field {
  function construct() {
    parent::construct();
    // FIXME is there really no way to do this conditionally?
    $this->additional_fields['entity_id'] = 'entity_id';
    $this->additional_fields['field_name'] = 'field_name';
  }
  function option_definition() {
    $options = parent::option_definition();
    $options['format'] = array('default' => 'serialized');
    return $options;
  }
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['format'] = array(
      '#type' => 'select',
      '#title' => t('Display format'),
      '#description' => t('How should the serialized data be displayed.'),
      '#options' => array(
        'serialized' => t('Full data (serialized)'),
        'unserialized' => t('Full data (array)'),
        'date' => t('Raw date, comma separated'),
        'joined' => t('Joined data (date + answer)'),
      ),
      '#default_value' => $this->options['format'],
    );
  }
  function render($values) {
    $value = $values->{$this->field_alias};
    if ($this->options['format'] === 'unserialized') {
      return check_plain(print_r(unserialize($value), TRUE));
    }
    if ($this->options['format'] === 'date') {
      $output = $this->makemeeting_views_render_dateonly($value);
      return $output;
    }
    if ($this->options['format'] === 'joined') {
      $output = $this->makemeeting_views_render_joined($values);
      return $output;
    }
    return $value;
  }
  function makemeeting_views_render_dateonly($value) {
    $value_arr = unserialize($value);
    $dates = array();
    $keys = array_keys($value_arr);
    foreach ($keys as $key) {
      $arr = explode(':', $key);
      $dates[] = $arr[0];
    }
    $rendered = implode(', ', $dates);
    return $rendered;
  }
  // following requires extra data from field table
  function makemeeting_views_render_joined($values) {
    $etid = $values->makemeeting_answers_entity_id;
    $field_name = 'field_data_' . $values->makemeeting_answers_field_name;
    $result = db_select($field_name, 'fi')
      ->fields('fi', array('field_meeting_choices', 'field_meeting_timezone'))
      ->condition('entity_id', $etid, '=')
      ->condition('deleted', 0, '=')
      ->execute()
      ->fetchAssoc();
    // eg: 08-11-2016 - chdate (array)
    //                  chsuggestions (array) - sugg:0, sugg:1...
    $choices_arr = unserialize($result['field_meeting_choices']);
    // eg: 08-11-2016:sugg:0 (Integer) 1
    $answers_arr = unserialize($values->makemeeting_answers_value);
    $timezone = $result['field_meeting_timezone'];
    $answers = array();
    foreach ($choices_arr as $date => $items) {
      $dt = new DateTime($date . ' 00:00:00', new DateTimeZone($timezone));
      $stamp = $dt->format('U');
      $offset = 0;
      $text = ' ';
      $dateformat = 'short';
      foreach ($items as $key => $suggs) {
        if ($key === 'chsuggestions') {
          foreach ($suggs as $index => $sugg) {
            if (isset($answers_arr[$date . ':' . $index])) {
              $answer = $answers_arr[$date . ':' . $index];
              if (!empty($sugg)) {
                if (preg_match('/^[0-2][0-9]:[0-5][0-9]/', $sugg, $matches)) {
                  $parts = explode(':', $matches[0]);
                  $offset = (3600 * $parts[0]) + (60 * $parts[1]);
                }
                else {
                  $dateformat = 'dateonly';
                  $text = ' (' . $sugg . ') ';
                }
              }
              else {
                $dateformat = 'dateonly';
              }
              switch ($answer) {
                case MAKEMEETING_YES:
                  $a = t('Yes');
                break;
                case MAKEMEETING_NO:
                  $a = t('No');
                break;
                case MAKEMEETING_MAYBE:
                  $a = t('Maybe');
                break;
                default:
                  $a = '';
              }
              $answers[] = format_date($stamp + $offset, $dateformat) . $text . $a;
            }
            else {
              $answers[] = format_date($stamp + $offset, $dateformat) . $text . t('No answer');
            }
          }
        }
      }
    }
    $value = array(
      '#theme' => 'item_list',
      '#items' => $answers,
      '#type' => 'ul',
      '#attributes' => array('class' => 'list-mm-answerd'),
    );
    return render($value);
  }
}
?>
